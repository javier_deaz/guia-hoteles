//FUNCIÓN PARA ACTIVAR LOS TOOLTIP Y POPOVERS 
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    // MODIFICANDO INTERVALO DE TIEMPO DE CAROUSEL
    $('.carousel').carousel({
        interval: 5000
    });
});
//MOSTRANDO EVENTOS DEL MODAL
$('#modalContactDecameron').on('show.bs.modal', function(e) {
    console.log('El modal se está mostrando')
    //QUITAR UNA CLASE SOBRE UNA ETIQUETA
    $('#btnContactDecameron').removeClass('btn-warning');
    //AGREGAR UNA CLASE SOBRE UNA ETIQUETA
    $('#btnContactDecameron').addClass('btn-outline-warning');
    //PROPIEDAD PARA DESHABILITAR UN BOTON
    $('#btnContactDecameron').prop('disabled', true);
});
$('#modalContactDecameron').on('shown.bs.modal', function(e) {
    console.log('El modal se mostró')
});
$('#modalContactDecameron').on('hide.bs.modal', function(e) {
    console.log('El modal se está ocultando')
});
$('#modalContactDecameron').on('hidden.bs.modal', function(e) {
    //PROPIEDAD PARA HABILITAR UN BOTON
    $('#btnContactDecameron').prop('disabled', false);
});